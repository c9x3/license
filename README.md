# [C9x3] License

My project(s) are licensed under the GNU Affero General Public License v3.0.

https://choosealicense.com/licenses/agpl-3.0/

# Monero (if you want to help support me) 🙂
86syNvdABvk8SP7Fi7KhDZE1GQ8UhfGCM7m2PABdbAaA9akbsJCbFmQ7bGbe59b4eU6pDqK7fQTErZxF4GsqXgGg9ZSksyo

# My Webpage

https://c9x3.neocities.org/

# What is Libre Software?

https://www.gnu.org/philosophy/open-source-misses-the-point.en.html

Free software, free society: Richard Stallman at TEDxGeneva 2014 - TEDx Talks - June 12, 2014

https://yewtu.be/watch?v=Ag1AKIl_2GM
